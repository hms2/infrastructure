variable "namespace_name" {
  description = "name of the namespace"
  type        = string
}

variable "annotations_name" {
  description = "annotation name"
  type        = string
}

variable "label" {
  description = "label"
  type        = string
}


variable "host" {
  description = "host"

}

variable "ca_cert" {
  description = "ca_cert"
  type        = string
  
}

variable "token" {
  description = "token"
 
}

