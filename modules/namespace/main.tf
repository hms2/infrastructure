resource "kubernetes_namespace" "terraform" {
  metadata {
    annotations = {
      name = var.annotations_name
    }

    labels = {
      mylabel = var.label
    }

    name = var.namespace_name
  }
}