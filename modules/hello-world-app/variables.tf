variable "app_name" {
  description = "name of the application"
  type        = string
}

variable "namespace" {
  description = "name of the namespace"
  type        = string
}

#labels
variable "labels_app" {
  description = "app name"
  type        = string
}

variable "labels_role" {
  description = "role"
  type        = string
}

variable "labels_tier" {
  description = "tier"
  type        = string
}

#replicas
variable "replicas" {
  description = "replicas"
}

#selectors
variable "selector_app" {
  description = "app name"
  type        = string
}

variable "selector_role" {
  description = "role"
  type        = string
}

variable "selector_tier" {
  description = "tier"
  type        = string
}

#template

variable "image" {
  description = "image and tag"
  type        = string
}

variable "container_name" {
  description = "container name"
  type        = string
}

variable "container_port" {
  description = "container_port"
}

variable "env_name" {
  description = "env name"
  type        = string
}

variable "env_value" {
  description = "env value"
  type        = string
}



#resources
variable "cpu" {
  description = "cpu value in milliCPU"
  type        = string
}
variable "memory" {
  description = "memory value in Mi"
}


#services

variable "type" {
  description = "type"
  type        = string
}

variable "port" {
  description = "port"
}

variable "target_port" {
  description = "target_port"
}


variable "host" {
  description = "host"
}

variable "ca_cert" {
  description = "ca_cert"
}

variable "token" {
  description = "token"
}

