resource "kubernetes_replication_controller" "deployment" {
  metadata {
    name      = var.app_name
    namespace = var.namespace
    labels    = {
         app  = var.labels_app
         role = var.labels_role
         tier = var.labels_tier
    }
  }

  spec {
    replicas = var.replicas

    selector = {
      app  = var.selector_app
      role = var.selector_role
      tier = var.selector_tier
    }

    template {
      container {
        image = var.image
        name  = var.container_name

        port {
          container_port = var.container_port
        }
       env {
          name  = var.env_name
          value = var.env_value
        }

        resources {
          requests {
            cpu    = var.cpu
            memory = var.memory
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "service" {
  metadata {
    name = var.app_name
    namespace = var.namespace

    labels = {
      app  = var.labels_app
      role = var.labels_role
      tier = var.labels_tier
    }
  }

  spec {
    selector = {
      app  = var.selector_app
      role = var.selector_role
      tier = var.selector_tier
    }

    type = var.type

    port {
      port        = var.port
      target_port = var.target_port
    }
  }
}



