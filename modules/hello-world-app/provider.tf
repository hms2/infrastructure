provider "kubernetes" {
  version = "~> 1.9"
  host                   = var.host
  cluster_ca_certificate = var.ca_cert
  token                  = var.token
  load_config_file       = false
}